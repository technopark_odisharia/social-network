#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use Local::Converter;
use Local::SocialNetwork::Database;

my $dbh = Local::SocialNetwork::Database->instance()->{ database };

parse_users($dbh);
parse_relations($dbh);

$dbh->disconnect();
