#!/usr/bin/env perl
use strict;
use warnings;
use 5.018;

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib "$FindBin::Bin/../lib";
use JSON::XS;
use Local::SocialNetwork;
use Local::SocialNetwork::Database;

no warnings 'experimental::smartmatch';

my $FRIENDS = 0;
my $NO_FRIENDS = 1;
my $HANDSHAKE = 2;
my %config;
my $mode;

sub parse_args {
    my ($command, $config_ref) = @_;

    GetOptions(
        'user=s@' => \@{$config_ref->{users}}
    );

    given ($command) {
        when ('friends') {
        	pod2usage(2) if scalar @{$config_ref->{users}} != 2;
        	$mode = $FRIENDS;
        }
        when ('nofriends') {
        	pod2usage(2) if scalar @{$config_ref->{users}} != 0;
        	$mode = $NO_FRIENDS;
        }
        when ('num_handshakes') {
        	pod2usage(2) if scalar @{$config_ref->{users}} != 2;
        	$mode = $HANDSHAKE;
        }
        default {
        	pod2usage(2)
        }
    }
}

sub get_json {
    return JSON::XS->new->utf8->pretty(1)->encode(shift)
}

parse_args(shift @ARGV, \%config);
given ($mode) {
    when ($FRIENDS)     { print get_json(mutual_friends(@{$config{users}})) }
    when ($NO_FRIENDS)  { print get_json(empty_friendlist()) }
    when ($HANDSHAKE)   { print get_json(handshakes(@{$config{users}})) }
};


__END__

=encoding utf8

=head1 NAME

sample - Using Getopt::Long and Pod::Usage

=head1 SYNOPSIS

social_network.pl [command]

Commands:

    friends --user XX --user YY         Общий список друзей пользователей c id XX и YY
    nofriends                           Список пользователей, у которых нет друзей
    num_handshakes --user XX --user YY  Количество рукопожатий между пользователями с id XX и YY

=head1 DESCRIPTION

Имеется некоторая база данных, содеражащая информацию о пользователях социальной
сети и связи, кто с кем дружит. Программа выдает различную информацию из этой базы данных.

=head2 Commands

=over 4

=item friends --user XX --user YY

Общий список друзей пользователей c id XX и YY

=item nofriends

Список пользователей, у которых нет друзей

=item num_handshakes --user XX --user YY

Количество рукопожатий между пользователями с id XX и YY

=back

=cut
