package Local::Converter;

use strict;
use warnings;
use IO::Uncompress::Unzip qw(unzip $UnzipError);
use FindBin;
use Exporter 'import';

our @EXPORT = qw(parse_users parse_relations);

sub parse_users {
	my $dbh = shift;
	my ($chunk, $counter) = (10000, 0);
	my $user_dat = new IO::Uncompress::Unzip "$FindBin::Bin/../etc/user.zip"
		or die "unzip failed: $UnzipError\n";
	my @arr;
	while (my $str = <$user_dat>) {
		my ($num, $first, $last) = split /\s+/, $str;
		push @arr, $num || '', $first || '', $last || '';
		if (++$counter % $chunk == 0) {
            $dbh->prepare(
                    "REPLACE INTO users (id, firstname, lastname) VALUES" .
                    ' (?, ?, ?),' x ($chunk - 1) .
                    ' (?, ?, ?)'
                )
                ->execute(@arr);
            undef @arr;
        }
	}
	if ($counter % $chunk) {
        $dbh->prepare(
                "REPLACE INTO users (id, firstname, lastname) VALUES" .
                ' (?, ?, ?),' x ($counter % $chunk - 1) .
                ' (?, ?, ?)')
            ->execute(@arr);
	}
}

sub parse_relations {
    my $dbh = shift;
    my ($chunk, $counter) = (100000, 0);
    my $relations_data = new IO::Uncompress::Unzip "$FindBin::Bin/../etc/user_relation.zip"
        or die "unzip failed\n";
    my @arr;
    while(my $line = <$relations_data>) {
        my ($user_id_1, $user_id_2) = split /\s+/, $line;
        push @arr, $user_id_1 || -1, $user_id_2 || -1;
        if(++$counter % $chunk == 0) {
            $dbh->prepare(
                'REPLACE INTO relations (user_id_2, user_id_1) VALUES' .
                ' (?, ?),' x ($chunk - 1) .
                ' (?, ?)')
                ->execute(@arr);
            undef @arr;
        }
    }
    if (scalar @arr) {
        $dbh->prepare(
                'REPLACE INTO relations (user_id_1, user_id_2) VALUES' .
                ' (?, ?),' x (scalar @arr / 2 - 1) .
                ' (?, ?)')
            ->execute(@arr)
    }
}

1;
