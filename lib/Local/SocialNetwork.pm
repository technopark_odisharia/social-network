package Local::SocialNetwork;

use strict;
use warnings;

use Exporter 'import';
use Local::SocialNetwork::User;
use Local::SocialNetwork::Config;
use Local::SocialNetwork::Database;
use Local::SocialNetwork::Memcached;

our @EXPORT = qw(mutual_friends empty_friendlist handshakes);

=encoding utf8

=head1 NAME

Local::SocialNetwork - social network user information queries interface

=head1 VERSION

Version 1.00

=cut

our $VERSION = '1.00';

=head1 SYNOPSIS

=cut

my $config = Local::SocialNetwork::Config->instance();
my $db = Local::SocialNetwork::Database->instance($config->{ db });
my $memcached = Local::SocialNetwork::Memcached->instance($config->{ memcached });

sub mutual_friends {
    my $id_XX = shift;
    my $id_YY = shift;

    my $user = Local::SocialNetwork::User->new(db => $db);

    my @friends_XX = @{ $user->new_by_id($id_YY)->get_friends() };
    my @friends_YY = @{ $user->new_by_id($id_XX)->get_friends() };
    my @mutual = grep {
        my $friend_XX = $_;
        grep { $_ == $friend_XX } @friends_YY
    } @friends_XX;

    my @mutual_out;
        push @mutual_out, $user->new_by_id($_)->get() foreach (@mutual);
    return \@mutual_out;
}

sub empty_friendlist {
    my $loners = Local::SocialNetwork::User->new(db => $db)->get_loners();
    return $loners;
}

sub handshakes {
    my $id_XX = shift;
    my $id_YY = shift;
    my $user = Local::SocialNetwork::User->new(db => $db);

    my $handshake_mc = $memcached->get("id1:$id_XX;id2:$id_YY");
    return { "num_handshakes" => $handshake_mc } if (defined $handshake_mc);

    if (!$user->has_friends($id_XX) or !$user->has_friends($id_YY)) {
        $db->disconnect();
        die("No friends.");
    }
    my ($handshakes, $i) = (0, 0);
    my %selected_h = ($id_XX => 1);
    while (!defined $selected_h{$id_YY}) {
        $selected_h{$_} = 1 foreach (@{$user->get_friends_by_list($id_YY, keys %selected_h) });
        $handshakes++;
    }
    $memcached->set("id1:$id_XX;id2:$id_YY", $handshakes);
    return { "num_handshakes" => $handshakes };
}

1;
