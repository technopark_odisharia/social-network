package Local::SocialNetwork::Config;

use strict;
use warnings;

use FindBin;
use YAML::XS 'LoadFile';
use base 'Class::Singleton';

sub _new_instance {
  my $pkg = shift;
  my $self  = bless {}, $pkg;
  my $filename_yaml = "$FindBin::Bin/../config/config.yaml";

  $self->{ config }  = LoadFile($filename_yaml);

  return $self->{ config };
}

1;
