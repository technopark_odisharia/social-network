package Local::SocialNetwork::Database;

use strict;
use warnings;

use DBI;
use base 'Class::Singleton';

my $instance = undef;

sub _new_instance {
    my ($class, $config) = @_;
    my $self  = bless { }, $class;

    my $dsn = "DBI:" . $config->{driver} . ":" . "database=" . $config->{db_name} .
        ";host=" . $config->{host} . ":" . $config->{port};
    my $attr = {
        mysql_enable_utf8   => $config->{mysql_enable_utf8},
        AutoCommit          => $config->{AutoCommit}
    };

    $self->{ database } = DBI->connect(
        $dsn, $config->{username}, $config->{password}, $attr
    ) || die "Cannot connect to database...\n";

    return $self->{ database }
}

1;
