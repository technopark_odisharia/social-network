package Local::SocialNetwork::Memcached;

use v5.10;
use strict;
use warnings;

use Cache::Memcached;

use base 'Class::Singleton';

sub _new_instance {
    my $class = shift;
    my $config = shift;
    my $self = bless { }, $class;

    $self->{ memcached } = Cache::Memcached->new(
        servers => [
            $config->{ servers }->{ host } . ":" . $config->{ servers }->{ port }
        ],
        namespace => $config->{ namespace }
    ) || die "Can not establish Memcached connection...\n";

    return $self->{ memcached }
}

1;
