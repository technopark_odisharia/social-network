package Local::SocialNetwork::User;

use strict;
use warnings;

use utf8;
use Mouse;

use Local::SocialNetwork::Database;
use Local::SocialNetwork::Memcached;

has id                  => ( is => 'ro', isa => 'Num', required => 0 );
has first_name          => ( is => 'ro', isa => 'Str', required => 0 );
has last_name           => ( is => 'ro', isa => 'Str', required => 0 );
has friends             => ( is => 'ro', required => 0 );
has db                  => ( is => 'ro', required => 1 );

sub new_by_id {
    my ($self, $id) = @_;
    my (@friends, $usr);
    for my $i ([2, 1], [1, 2]) {
        my $sql =   'SELECT users.id, users.firstname, users.lastname, relations.user_id_' .
                    $i->[0] . ' from users INNER JOIN relations ' .
                    'ON users.id = relations.user_id_' . $i->[1] . ' WHERE users.id = ?';
        my $sth = $self->db->prepare($sql);
        $sth->execute($id);
        $usr = $sth->fetchrow_hashref();
        push @friends, map { $_->[3] } @{ $sth->fetchall_arrayref() };
    }
    return $self->new(
        id          => $id,
        first_name  => $usr->{firstname},
        last_name   => $usr->{lastname},
        friends     => \@friends,
        db          => $self->db,
    );
}

sub get_friends {
    my $self = shift;
    return $self->{friends};
}

sub get_full_name {
    my $self = shift;
    return $self->{first_name} . " " . $self->{last_name};
}

sub get {
    my $self = shift;
    return {id          => $self->{id},
            full_name   => $self->get_full_name()}
}

sub get_loners {
    my $self = shift;

    my $sth = $self->db->prepare(
        'SELECT * FROM users WHERE ' .
        'id NOT IN (SELECT user_id_1 FROM relations) AND ' .
        'id NOT IN (SELECT user_id_2 FROM relations)'
    );
    $sth->execute();
    my $loners = $sth->fetchall_hashref('id') || '';
    return $loners;
}

sub get_friends_by_list {
    my ($self, $find_id, @list) = @_;
    my %h;
    my $chunk_num = 1000;
    my @chunks = group_by ($chunk_num, @list);
    foreach my $chunk (@chunks) {
        my @res;
        for my $i ([1, 2], [2, 1]) {
            my $sth = $self->db->prepare('SELECT user_id_' . $i->[1] .' FROM ' .
                '(SELECT * FROM users INNER JOIN ' .
                    '(SELECT user_id_1, user_id_2 FROM relations ) rel ' .
                ' ON users.id = rel.user_id_' . $i->[0] .
                ' WHERE user_id_' . $i->[0] . ' = ?' .
                (' OR user_id_' . $i->[0] . ' = ?') x ((scalar @$chunk) - 1) . ') s');
            $sth->execute(@$chunk);
            push @res, map { $_->[0] } @{$sth->fetchall_arrayref()};
        }
        foreach my $id (@res) {
            $h{$id} = 1;
            return [ keys %h ] if $find_id == $id;
        }
    }
    return [ keys %h ];
}

sub group_by {
    my $n = shift;

    my @groups;
    push @groups, [ splice @_, 0, $n ] while scalar @_;
    return @groups;
}

sub has_friends {
    my ($self, $id) = @_;
    my @friends;
    for my $i ([2, 1], [1, 2]) {
        my $sth = $self->db->prepare(
            'SELECT relations.user_id_' . $i->[0] .
            ' FROM relations WHERE user_id_' . $i->[1] . ' = ?'
        );
        $sth->execute($id);
        push @friends, map { $_->[0] } @{ $sth->fetchall_arrayref() };
    }
    return (scalar @friends) ? 1 : 0;
}

1;
