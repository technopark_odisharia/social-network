CREATE TABLE IF NOT EXISTS users (
  id              SERIAL              NOT NULL  UNIQUE   PRIMARY KEY,
  firstname       VARCHAR(256)        NOT NULL,
  lastname        VARCHAR(256)        NOT NULL
) ENGINE = INNODB CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS relations (
  id              SERIAL              NOT NULL  PRIMARY KEY,
  user_id_1       INT UNSIGNED        NOT NULL,
  user_id_2       INT UNSIGNED        NOT NULL

  -- CONSTRAINT user_id_1_users_id FOREIGN KEY
  --   (user_id_1)
  --   REFERENCES users(id)
  --   ON DELETE CASCADE
  --   ON UPDATE CASCADE,
  -- CONSTRAINT user_id_2_users_id FOREIGN KEY
  --   (user_id_2)
  --   REFERENCES users(id)
  --   ON DELETE CASCADE
  --   ON UPDATE CASCADE
) ENGINE = INNODB CHARACTER SET utf8;
