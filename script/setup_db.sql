CREATE DATABASE IF NOT EXISTS socialnetwork;

CREATE USER IF NOT EXISTS 'socialnetwork'@'localhost' IDENTIFIED BY 'social_social';
GRANT ALL ON socialnetwork.* TO 'socialnetwork'@'localhost';
FLUSH PRIVILEGES;

USE socialnetwork;

SOURCE script/mysql/create_tables.sql;
